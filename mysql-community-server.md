*** STEPS *** 
1. https://dev.mysql.com/downloads/
2. Click MYSQL Community Server
3. Click download -> x86 .dmg file
4. Install the mysql dmg package
5. Open the terminal
<!-- What the command below does is, it creates a file alias(mysql) in the default execution path so that we can just be typing mysql to start using it -->
<!-- sudo ln -s -> Command to create a symbolic link or a file alias(mysql) as a super user coz of sudo -->
<!-- /usr/local/mysql/bin/mysql -> This is the path to the actual mysql command line -->
<!-- /usr/local/bin -> This is in the default execution path -->

6. Then type: sudo ln -s /usr/local/mysql/bin/mysql /usr/local/bin
7. Then type: mysql -u root -p ... then hit enter ... then, enter the mysql password (root4321).
8. Then verify that mysql installed correctly by typing: show databases;
9. Then type: quit and hit enter 