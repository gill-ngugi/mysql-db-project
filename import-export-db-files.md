*** TO NOTE: ***
Database -> Collection of schemas.
Schema -> Collection of tables.

*** STEPS - Import DB Files ***
1. Open workbench and the local instance (3306).
2. Administration tab -> data import/restore -> click import from self-contained file 
<!-- What's on album-mysql.sql is a set of SQL commands that will import the DB into the system. -->
3. Then click on the ellipsis button to the right open exercise files -> databases -> album-mysql.sql
4. Then select the import progress tab -> start import 
5. Repeat the above process to import the other two databases from the exercise files.
6. Then start chapter one.