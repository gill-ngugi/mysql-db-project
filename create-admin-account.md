*** STEPS ***
1. Go to administration tab
2. Click users and privileges.
3. Then click ADD ACCOUNT at the bottom of the page.
4. Login Tab: 
    a. Login name -> admin
    b. Auth type -> caching sha-2
    c. Limit to hosts matching -> localhost
    d. Password -> localhost4321!
5. Admin roles tab: Select DBA to grant all privileges.
6. Then click Apply.
7. Verify the user is added to the left.

-------
THEN
-------
Click on the home icon.
Click on the + icon to add a new connection.
Connection name -> admin
Connection method -> Standard (TCP/IP)
Username -> admin 
Click OK






