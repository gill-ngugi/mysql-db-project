*** STEPS *** 
1. https://dev.mysql.com/downloads/
2. Click MYSQL Workbench
3. Then download the x86 .dmg file
4. Open the MYSQL Workbench app and go to preferences.
5. Click SQL editor and uncheck safe upadates at the bottom of the screen
6. Click on Local instance 3306, enter password and you're in!
7. Check status: Administration tab -> Management -> Server status
8. Under Query1 tab -> Type SHOW DATABASES; -> Click command+enter.