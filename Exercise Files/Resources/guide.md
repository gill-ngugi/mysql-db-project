*** GUIDE ***
----------------------------
Wednesday, 27th April 2022
----------------------------
A. DATABASES
i. MYSQL 
	MySQL Installation and Configuration … https://www.linkedin.com/learning/mysql-installation-and-configuration?leis=LTI&u=70023106 
	MySQL Essential Training … https://www.linkedin.com/learning/mysql-essential-training-2?leis=LTI&u=70023106
	MySQL Advanced Topics … https://www.linkedin.com/learning/mysql-advanced-topics?leis=LTI&u=70023106
	SQL Essential Training … https://www.linkedin.com/learning/sql-essential-training-3?leis=LTI&u=70023106
