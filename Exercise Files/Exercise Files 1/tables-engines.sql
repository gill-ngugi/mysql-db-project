-- TYPES OF ENGINES
-- InnoDB, MyISAM, Memory, CSV, Blackhole, Archive, Merge, Performace schema, Federated.

-- Standard for modern day reliability - ACID.
--      ACID -> Atomicity, Consistency, Isolation, Durability
--      InnoDB is fully ACID compliant.      

/* InnoDB engine is the default engine for modern versions of MYSQL. 
--      It is designed for high performance and high reliability.
--      InnoDB can be used for temporary storage. This is more recommended as compared to memory engine.
--      This is because InnoDB is not constrained by available memory.
*/

/* MyISAM is a legacy engine that's mostly used for compatibility with older versions of MYSQL. 
--      This includes older versions before MYSQL V5.5 released in  2010.
--      ISAM - Indexed Sequential Access Method. It doesnt support transactions and foreign keys.
*/

-- Memory Engine stores its contents in temporary memory, but the data is vulnerable to crashes,
--      power outages and hardware issues. Memory engine is constrained by available memory on the server.

-- FIRST COMMANDS.
SHOW engines;
SHOW databases;
USE world;
SHOW tables;
DESCRIBE Country;
SELECT * FROM City;


-- create table with engine
USE scratch;
DROP TABLE IF EXISTS test;
CREATE TABLE IF NOT EXISTS test ( 
    id INT AUTO_INCREMENT PRIMARY KEY,
    cname VARCHAR(128),
    localname VARCHAR(128)
) ENGINE = InnoDB;
INSERT INTO test (cname, localname) SELECT name, localname FROM world.country;
SELECT COUNT(*) FROM test;
SELECT * FROM test;
SELECT table_name, engine FROM information_schema.tables WHERE table_schema = 'scratch';
DROP TABLE IF EXISTS test;
