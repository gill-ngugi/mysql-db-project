-- test installed databases
-- In MYSQL Workbench, to run two commands at once, click: shift+command+enter

USE world;
SHOW tables;

USE scratch;
SHOW tables;

USE album;
SHOW tables;

USE album;
SELECT * FROM album;
