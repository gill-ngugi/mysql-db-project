-- 01 value functions

SELECT ABS(-47); -- Absolute is always positive. Returns -> 47
SELECT ABS(-47.73); -- Returns 47.73

SELECT CEILING(12.2); -- Returns 13
SELECT CEIL(12.2); -- Same as above

SELECT FLOOR(12.9); -- Returns 12

SELECT ROUND(17.5); -- Returns 18
SELECT ROUND(17.4); -- Returns 17

SELECT TRUNCATE(42.973, 1); -- Returns 42.9
SELECT TRUNCATE(42.973, 2); -- Returns 42.97
-- This one truncates to the left of the decimal and fills the remaining digits with zero
SELECT TRUNCATE(99942.973, -2); -- Returns 99900

-- 02 math funcitons

SELECT PI(); -- Returns '3.141593'
SELECT PI() + 0.000000000000000; -- Returns pi to the 15th decimal

SELECT POWER(8, 2); -- Returns 8 to the power of 2.
SELECT POW(8, 2); -- Returns same as above

SELECT SQRT(64); -- Returns 8 -- Squareroot of 64.
SELECT POWER(4096, 1/4); -- Returns fourth root of 4095

SELECT RAND(); -- Returns a random number
SELECT RAND(42);

-- 03 trigonometry

SELECT SIN(2);
SELECT ASIN(.2);
SELECT COS(PI());
SELECT ACOS(.5);
SELECT TAN(PI());
SELECT ATAN(2);
SELECT COT(12);

-- 04 logarithms

SELECT LN(2); -- Returns natural logarithm of the number 2
SELECT LOG(2); -- Returns the same as above
-- Returns the logarithm of the second argument to the base of the first argument
SELECT LOG(10, 100);
SELECT LOG(2, 65535);
SELECT LOG2(65535); -- Returns the logarithm of the argument to the base of two
SELECT LOG10(100); -- Returns the logarithm of the argument to the base of ten
SELECT EXP(1); -- Returns the value of e raised to the power of the provided argumernt. Exp stands for Exponential.

-- 05 degrees and radians

SELECT DEGREES(PI()); -- Converts the argument to degrees.
SELECT RADIANS(180); -- Converts the argument to radians.
