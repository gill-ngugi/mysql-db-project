-- 01 string comparisons

USE world;
-- The below statement returns the names of countries which have their fist character as any xter, and their second xter as an 'a'.
SELECT Name FROM country WHERE Name LIKE '_a%' ORDER BY Name; 
-- The below statement returns the names of all countries in order of name(a-z) up until France. 
SELECT Name FROM country WHERE STRCMP(Name, 'France') <= 0 ORDER BY Name;

-- 02 regular expressions

USE world;
-- The statement below returns countries whose names end with a 'y'.
SELECT Name FROM country WHERE Name RLIKE 'y$' ORDER BY Name;
-- The statement below returns countries whose names have either xters x or y followed by a or i.
SELECT Name FROM country WHERE Name RLIKE '[xy][ai]' ORDER BY Name;

-- 03 string concatenation

SELECT 'This ' || 'and ' || 'that';
SELECT CONCAT('This ', 'and ', 'that');
SELECT CONCAT('Love', ' ', 'is', ' ', 'all', ' ', 'you', ' ', 'need');
SELECT CONCAT('one', 'two');
SELECT CONCAT('string', 42);
SELECT CONCAT(42, 42);

-- 04 numeric conversions

SELECT 32742; -- will return the number as is.
SELECT HEX(32742); -- converts the number to base 16.
SELECT OCT(32742); -- converts the number to base 8 (no number is higher than 7).
SELECT BIN(32742); -- converts to binary, will return number in zeros and ones.

SELECT CONV('32742',10,16); -- will convert the number from base 10 to base 16.
SELECT CONV('7FE6',16,10); -- will convert the number from base 16 to base 10.
SELECT CONV('28K6',24,10);

-- 05 trimming and padding

USE scratch;
-- This won't return anything coz of the extra spaces at the beginning and at the end.
SELECT * FROM customer WHERE name LIKE '  Bill Smith  '; 
-- This will return Bill Smith because the spaces will be trimmed with the TRIM keyword.
SELECT * FROM customer WHERE name LIKE TRIM('  Bill Smith  ');
SELECT CONCAT(':', TRIM('  Bill Smith  '), ':'); -- This trims the spaces on either side of Bill Smith.
SELECT CONCAT(':', RTRIM('  Bill Smith  '), ':'); -- This trims the spaces on the right side.
SELECT CONCAT(':', LTRIM('  Bill Smith  '), ':'); -- This trims the spaces on the left side.

SELECT CONCAT(':', TRIM('x' FROM 'xxxBill Smithxxx'), ':'); -- This trims the exes and will return :Bill Smith :

SELECT LPAD('Price', 20, '.'); -- This will return a total of 20 xters like '.......Price' -> the dots will be 15 and price has 5 xters
SELECT LPAD('Price', 20, '. '); -- This will return a total of 20 xters like ' -> the dots will be 8, spaces 7 and price has 5 xters
SELECT RPAD('Price', 20, '. '); -- 'Price . . . . . . . '

-- 06 case conversion

USE scratch;
SELECT UPPER(name) FROM customer; -- Returns xters all in uppercase -> BILL SMITH
SELECT UCASE(name) FROM customer; -- Same as above
SELECT LOWER(name) FROM customer; -- Returns xters all in lowercase -> bill smith
SELECT LCASE(name) FROM customer; -- Same as above 
-- The statement below returns names in the name column -> (from the first xter, just the first xter(name, 1, 1)) will be uppercase.
-- Then the xters after the first xter as lower case. Like so -> Bill smith
SELECT CONCAT(UPPER(SUBSTRING(name, 1, 1)),LOWER(SUBSTRING(name, 2))) FROM customer;

-- 07 substring

SELECT SUBSTRING('this is a string', 6); -- Returns all xters after postion 6: 'is a string'
SELECT SUBSTR('this is a string', 6); -- Same as above.
SELECT SUBSTR('this is a string', 6, 4); -- Returns the first four xters after position 6: 'is a'
SELECT SUBSTR('this is a string', -6); -- Returns all xters after position six from the end of the string -> 'string'
SELECT SUBSTR('this is a string', -6, 4); -- Returns four xters after position six from the end of the string -> 'stri'

-- The second parameter is the delimiter, a space ' ' and the third parameter specifies, 
-- return the string content up to the first delimiter. This will return -> this
SELECT SUBSTRING_INDEX('this is a string', ' ', 1); 
-- Returns string content up until the second delimiter, returns -> this is
SELECT SUBSTRING_INDEX('this is a string', ' ', 2); 
-- Returns string content up until the second delimiter from the end, returns -> a string
SELECT SUBSTRING_INDEX('this is a string', ' ', -2);

-- 08 soundex

SELECT SOUNDEX('bill'), SOUNDEX('bell');
SELECT SOUNDEX('acton'), SOUNDEX('action');
SELECT SOUNDEX('acting'), SOUNDEX('action');

SELECT 'bill' SOUNDS LIKE 'boil', 'bill' SOUNDS LIKE 'phil';



