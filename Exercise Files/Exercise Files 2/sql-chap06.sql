-- 01 string concatenation

SELECT 'string1' || 'string2'; -- standard not mysql
SELECT TRUE || FALSE;          -- non-standard mysql ... Returns a 1 for True
SELECT TRUE OR FALSE;          -- returns a 1. Works the same as above.
SELECT CONCAT('string1', 'string2'); -- returns 'string1string2'

-- 02 quote marks

-- Single quotes is the standard in SQL for referring to Strings.
SELECT 'this is a string';
-- Double quotes are not the SQL standard but are still supported in MYSQL as a concession to legacy code.
SELECT "this is a string";

USE scratch;
-- This is not standard and will NOT return the correct values from the name and address columns from the customer table.
SELECT "name", "address" FROM customer;
-- This will return the correct values from the name and address columns from the customer table.
SELECT `name`, `address` FROM customer; 

-- 03 the modulo operator

-- This is non-standard, but MYSQL supports this.
SELECT 47 / 3; -- Returns 15.6667 ... 
-- This is the standard SQL way for division but MYSQL doesn't support this method.
SELECT DIV(47,3);  -- It should return 15.6667,
-- This is non-standard. MYSQL supports this.
SELECT 47 DIV 3; -- Returns 15, without the remainder
-- This is non-standard. MYSQL supports this.
SELECT 47 MOD 3; -- Returns the remainder of the division -> 2
-- This is standard. MYSQL supports this.
SELECT MOD(47,3); -- Returns -> 2
-- This is non-standard. MYSQL supports this.
SELECT 47 % 3;      

-- 04 comments

1. --SQL -> standard SQL comment, MYSQL doesn't support this, as it requires a space after the double dash.
2. -- SQL standard comment -> This is supported by MYSQL.
3. # non-standard MySQL comment -> This is supported by MYSQL.
4. #non-standard MySQL comment -> This is supported by MYSQL.
5. /*
    SQL standard 
    multi-line comment
    is supported by MYSQL.
   */
6. -- The statement below will return 3 in MYSQL and 1 in other SQL Engines.
   -- This is only supported in MYSQL.
   SELECT 1 /*! + 2 */ ;
7. /*!
    SELECT 'MySQL executable comment' AS `Say what?!`
   */ ;



